unit Unit4;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,web3.eth.erc20,web3,web3.eth.types,
  FMX.Controls.Presentation, FMX.StdCtrls;


type
  TForm4 = class(TForm)
    Transfer: TButton;
    procedure TransferClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.fmx}

procedure TForm4.TransferClick(Sender: TObject);

begin
  var ERC20 := TERC20.Create(TWeb3.Create(
  RSK_test_net,
  'https://public-node.testnet.rsk.co'),
  '0x108aB2c128e423A6539aC3bdCb25c6f4fFA3497E');   // TST smart contract address
  try
    ERC20.Transfer(
      '6e7138f18921d11168ff29d0a4a3da7a0e5777eff083e3de17e4aee05a53bcdd', // from private key
      '0x45870dCfE12220781f15ED090d5F2CA88E5002A5',                       // to public key
      5,
      procedure(rcpt: TTxHash; err: IError)
      begin
        TThread.Synchronize(nil, procedure
        begin
          if Assigned(err) then
            MessageDlg(err.Message, TMsgDlgType.mtError, [TMsgDlgBtn.mbOK], 0)
          else
            MessageDlg(string(rcpt), TMsgDlgType.mtInformation, [TMsgDlgBtn.mbOK], 0);
        end);
      end);
  finally
    ERC20.Free;
  end;
end;




end.
